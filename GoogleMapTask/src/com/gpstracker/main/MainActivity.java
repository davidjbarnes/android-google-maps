package com.gpstracker.main;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.gpstracker.fragments.MapCustomFragment;
import com.gpstracker.service.LocationService;

public class MainActivity extends FragmentActivity {
	private final int MAPFRAGMENT = 0;
	
	private FragmentManager fragmentManager = null;
	private FragmentTransaction fragmentTransaction = null;
    private MapCustomFragment mapFragment = null;

    /** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		initData();
	}

	public void initData() {
		fragmentManager = getSupportFragmentManager();	
	    fragmentTransaction = fragmentManager.beginTransaction();
	    mapFragment = MapCustomFragment.newInstance(MAPFRAGMENT);
	    mapFragment.setRetainInstance(true);
	    fragmentTransaction.add(R.id.parentRelativeLayout, mapFragment);
        fragmentTransaction.commit();
        Intent service = new Intent(this, LocationService.class);
        startService(service);
    }
	public void onResume() {
		super.onResume();
		if (chkGpsService()) {
	        showMapFragment();
		}         
	}
	private boolean chkGpsService() {
		String gs = android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
		
		Log.i("chkGpsService", "get GPS Service");
		
		if ( gs.indexOf("gps", 0) < 0 ) {
			AlertDialog.Builder gsDialog = new AlertDialog.Builder(MainActivity.this);
			gsDialog.setTitle("GPS Status Off");
			gsDialog.setMessage("Turn on GPS");
			gsDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					intent.addCategory(Intent.CATEGORY_DEFAULT);
					startActivity(intent);
				}
			}).create().show();
			return false;
		}
		else {
			return true;
		}
	}
	public void showMapFragment() {
		fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.show(mapFragment);
		fragmentTransaction.commit();
	}
	
}
