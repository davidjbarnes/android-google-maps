 package com.gpstracker.fragments;

import android.app.ProgressDialog;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.gpstracker.constants.LocationModel;
import com.gpstracker.constants.LocationModel.OnLocationStateListener;
import com.gpstracker.main.R;

public class MapCustomFragment extends Fragment implements OnClickListener, OnLocationStateListener {
    private GoogleMap mMap;
    private ProgressDialog dialog = null;
	public static MapCustomFragment newInstance(int index) {
		MapCustomFragment f = new MapCustomFragment();
		
		Bundle args = new Bundle();
		args.putInt("index", index);
		f.setArguments(args);
		
		return f;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);

		initView();
		initilizeMap();
		LocationModel.getInstance().setListener(this);
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View layout = inflater.inflate(R.layout.fragment_map, container, false);
		if (dialog == null) {
			dialog = new ProgressDialog(getActivity());
			dialog.setMessage("Please wait...");
			dialog.show();	
		}
		return layout;
	}
	public void initView() {
		
	}
	@Override
	public void onPause() {
		super.onPause();
	}
	public void onResume() {
		super.onResume();
		initilizeMap();
	}
	public void onClick(View v) {
		switch(v.getId()) {
		}
	}
	public void initilizeMap() {
		mMap = ((MapFragment) getActivity().getFragmentManager().findFragmentById(R.id.map)).getMap();
		mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
		mMap.animateCamera( CameraUpdateFactory.zoomTo( 12.0f ) ); 
	}
	@Override
    public void stateChanged() {
		
		Location currentLocation = LocationModel.getInstance().getState();
        final LatLng currentPos = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
        
		mMap.clear();
		mMap.addMarker(new MarkerOptions()
		               .title("You are here")
	      			   .position(currentPos));
		mMap.moveCamera(CameraUpdateFactory.newLatLng(currentPos));
		if (dialog != null) {
			dialog.dismiss();
			dialog = null;
			mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentPos, 12.0f)); 
		}
		
    }
	
}