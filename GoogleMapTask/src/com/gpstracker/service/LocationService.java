package com.gpstracker.service;

import android.app.Service;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.gpstracker.constants.LocationModel;

public class LocationService extends Service {
	public static Location currentLocation_;
	public LocationManager locationManager_;
	
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public void onCreate() {

	}

	@Override
	public void onStart(Intent intent, int startId) {
		getLocation();
	}

	@Override
	public void onDestroy() {

	}
	private void getLocation() {
		locationManager_ = (LocationManager) getSystemService(LOCATION_SERVICE);
		
		Criteria criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		criteria.setPowerRequirement(Criteria.POWER_LOW);
		criteria.setAltitudeRequired(false);
		
		criteria.setBearingRequired(false);
		criteria.setSpeedRequired(false);
		criteria.setCostAllowed(true);
		 
		if (locationManager_ == null)
			return;
		currentLocation_ = locationManager_.getLastKnownLocation("gps");
		
		if (currentLocation_ == null) {
			currentLocation_ = locationManager_.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		} 
		
		locationManager_.requestLocationUpdates(LocationManager.GPS_PROVIDER, 20000, 0, gpsLocationListener);
		if (currentLocation_ != null) {
			LocationModel.getInstance().changeState(currentLocation_);
		}
	}
	
	private LocationListener gpsLocationListener = new LocationListener() {

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			
			Log.e("LOCATION", "onStatusChanged");
		}

		@Override
		public void onProviderEnabled(String provider) {
			
			Log.e("LOCATION", "onProviderEnabled");
		}

		@Override
		public void onProviderDisabled(String provider) {
			
			Log.e("LOCATION", "onProviderDisabled");
		}

		@Override
		public void onLocationChanged(Location location) {
			Log.e("LOCATION", "onLocationChanged");
			currentLocation_.set(location);
			LocationModel.getInstance().changeState(currentLocation_);
		}
	};
	
}
